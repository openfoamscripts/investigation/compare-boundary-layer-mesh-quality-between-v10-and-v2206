# Compare Boundary Layer Mesh Quality between v10 and v2206

#### AUTHOR: M.TANAKA
#### DATE: 2022.11.20

## Description

I compared the mesh quality at the boundary layer between v10(openfoam.org) and v2206(openfoam.com). 

| ![](images/featureAngle060_v10.png) | ![](images/featureAngle060_v2206.png) |
| :---------------------------------: | :-----------------------------------: |
| **OpenFOAM v10**                    | **OpenFOAM v2206**                    |

Apparently, v10 creates better mesh around corners. Maybe I should switch from the ESI version (v2206) to the foundation version (v10).

## Usage

### v2206

1. Load OpenFOAM environment. add line ```source /usr/lib/openfoam/openfoam2206/etc/bashrc``` to ```~/.bashrc```, and remove the other line.

2. execute ```source ~/.bashrc```

3. execute ```./Allrun.v2206```

:warning: this simulation will meet floating point exception while pimpleFoam.

### v10

1. Load OpenFOAM environment. add line ```source /opt/openfoam10/etc/bashrc``` to ```~/.bashrc```, and remove the other line.

2. execute ```source ~/.bashrc```

3. execute ```./Allrun.v10```

:ok_hand: this simulation ends successfully.

## Ref.

This case folder is based on [rotatingFanInRoom](https://develop.openfoam.com/Development/openfoam/-/tree/OpenFOAM-v2206/tutorials/incompressible/pimpleFoam/RAS/rotatingFanInRoom) tutorial.